import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appRendererHighlight]'
})
export class RendererHighlightDirective implements OnInit {

  @Input() defaultColor: string;
  @Input() orange: string;
  @HostBinding('style.backgroundColor') color: string;

  constructor(private element: ElementRef, private renderer: Renderer2){}

  ngOnInit(){
    this.color = this.defaultColor;
  }

  @HostListener('mouseenter') onmouseenter(event: Event){
    this.color = 'red';
  }

  @HostListener('mouseleave') onmouseleave(event: Event){
    this.color = this.orange;
  }

  @HostListener('click') onclick(event: Event){
    this.color = 'green';
  }

}
