import { Component, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UsersComponent implements OnInit {

  title: string = "User Registration";
  userList = [];

  value = 15;

  constructor() { }

  ngOnInit(): void {
  }

  onSubmitRegister(event: string){
    this.userList.push(event);
  }


}
